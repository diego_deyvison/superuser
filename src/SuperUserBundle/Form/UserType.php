<?php

namespace SuperUserBundle\Form;

use SuperUserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

Class UserType extends AbstractType
{   
    public function buildForm(FormBuilderInterface $builder, array $options )
    {
        $builder
        ->add('name' , TextType::class , array('label' => 'Nome'))
        ->add('email' , EmailType::class , array('label' => 'E-mail'))
        ->add('avatar', FileType::class, array('label' => 'Foto (PNG ou Jpg)'))
        ->add('plainPassword', RepeatedType::class, array(
            'type' => PasswordType::class,
            'first_options'  => array('label' => 'Senha'),
            'second_options' => array('label' => 'Repita a senha'),
        ))
        ->add('bornDate', DateType::class , array(
            'label' => 'Data de Nascimento',
            'format' => 'dd-MM-yyy',
            'years' => range(date('Y') , date('Y') -120)
        ))
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }

}