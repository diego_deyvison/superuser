<?php

namespace SuperUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/" , name="default")
     */
    public function indexAction()
    {
        $users = null;
        
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('SuperUserBundle\Entity\User')->getUsersByCreatedDate();
        
        
        return $this->render('SuperUserBundle::index.html.twig', array(
            'users' => $users
        ));
    }

    /**
     * @Route("/user/{id}", name="user_detail")
     */
    public function detailsAction($id){

        $em = $this->getDoctrine()->getManager();
        
        $user= $em->getRepository('SuperUserBundle\Entity\User')->findOneBy(array('id' => $id));

        if (!$user) {
            throw $this->createNotFoundException('Usuário não encontrado');
        }

        return $this->render('SuperUserBundle::detail.html.twig', array(
            'user' => $user
        ));

    }
    
}
