<?php 

namespace SuperUserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends Controller
{
    /**
     * @Route("/login" , name="login"   )
     */
    public function loginAction(Request $request){
        
        $authUtils = $this->get('security.authentication_utils');
    
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();
    
        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();
    
        return $this->render('SuperUserBundle::security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
        
    }

    /**
     * @Route("/logout" , name="user_logout")
     */
    public function logoutAction(Request $request){

    }

}