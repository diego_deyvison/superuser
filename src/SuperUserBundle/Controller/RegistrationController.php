<?php

namespace SuperUserBundle\Controller;

use SuperUserBundle\Form\UserType;
use SuperUserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends Controller
{
    /**
     * @Route("/cadastro", name="user_registration")
     */
    public function registerAction(Request $request)
    {
        
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            //salvando a imagem
            $file = $user->getAvatar();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move(
                $this->getParameter('uploads_directory').'/user',
                $fileName
            );
            $user->setAvatar($this->getParameter('uploads_path').'/user/'.$fileName);
            

            //editando o username e password
            $user->setUsername($user->getEmail());
            $passwordEncoder = $this->get('security.password_encoder') ;
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'notice',
                'Cadastro realizado com sucesso!'
            );

            return $this->redirectToRoute('login');
        }

        return $this->render(
            'SuperUserBundle::cadastro.html.twig',
            array('form' => $form->createView())
        );
    }
}